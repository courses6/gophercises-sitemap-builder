module gitlab.com/courses6/gophercises-sitemap-builder

go 1.14

require (
	github.com/stretchr/testify v1.5.1
	gitlab.com/courses6/gophercises-html-link-parser v0.0.0-20200516130515-e4776be83ba8
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120
)

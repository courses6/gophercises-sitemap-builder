package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/courses6/gophercises-html-link-parser/domain/linkparser"
	"gitlab.com/courses6/gophercises-sitemap-builder/sitemap"
	"golang.org/x/net/html"
)

var (
	defaultURL      = "https://gophercises.com/"
	defaultMaxDepth = 3
)

// HTMLParser ...
type HTMLParser struct{}

// Parse ...
func (parser *HTMLParser) Parse(reader io.Reader) (*html.Node, error) {
	return html.Parse(reader)
}

func main() {
	url := flag.String("url", defaultURL, "the url the links are extracted from")
	maxDepth := flag.Int("depth", defaultMaxDepth, "the maximum number of links deep to traverse")
	flag.Parse()

	fmt.Printf("Trying to get links from: %s\n", *url)

	urls := bfs(*url, *maxDepth)
	// urls := getUrls(*url)
	fmt.Printf("%+q", urls)
}

func bfs(urlStr string, depth int) []string {
	seen := make(map[string]struct{})
	var q map[string]struct{}
	nq := map[string]struct{}{
		urlStr: {},
	}
	for i := 0; i < depth; i++ {
		q, nq = nq, make(map[string]struct{})
		if len(q) == 0 {
			break
		}
		for url := range q {
			if _, ok := seen[url]; ok {
				continue
			}
			seen[url] = struct{}{}
			for _, link := range getUrls(url) {
				if _, ok := seen[url]; !ok {
					nq[link] = struct{}{}
				}
			}
		}
	}

	ret := make([]string, 0)
	for k := range seen {
		ret = append(ret, k)
	}
	return ret
}

func getUrls(urlStr string) []string {
	resp, err := http.Get(urlStr)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	reqURL := resp.Request.URL
	baseURL := &url.URL{
		Scheme: reqURL.Scheme,
		Host:   reqURL.Host,
	}
	base := baseURL.String()

	parser := &HTMLParser{}
	usecase := linkparser.NewExtractLinksUseCase(parser)
	reader := bytes.NewReader(body)
	links, err := usecase.Execute(reader)
	if err != nil {
		panic(err)
	}
	return sitemap.FilterOutExtrenalDomainUrls(
		sitemap.LinksToUrls(links, base),
		predicate(base),
	)
}

func predicate(base string) func(string) bool {
	return func(urlStr string) bool {
		return strings.HasPrefix(urlStr, base)
	}
}

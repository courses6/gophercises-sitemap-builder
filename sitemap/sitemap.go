package sitemap

import (
	"strings"

	"gitlab.com/courses6/gophercises-html-link-parser/domain/linkparser"
)

// LinkToString ...
func LinkToString(link linkparser.Link, base string) string {
	var ret string
	switch {
	case strings.HasPrefix(link.Href, "/"):
		ret = base + link.Href
	case strings.HasPrefix(link.Href, "http"):
		ret = link.Href
	}
	return ret
}

// LinksToUrls ...
func LinksToUrls(links []linkparser.Link, base string) []string {
	var urls []string
	for _, l := range links {
		urls = append(urls, LinkToString(l, base))
	}
	return urls
}

// FilterOutExtrenalDomainUrls ...
func FilterOutExtrenalDomainUrls(urls []string, predicate func(urlStr string) bool) []string {
	var filtered []string
	for _, url := range urls {
		if predicate(url) {
			filtered = append(filtered, url)
		}
	}
	return filtered
}

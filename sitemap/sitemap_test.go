package sitemap

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/courses6/gophercises-html-link-parser/domain/linkparser"
)

func TestLinkToStringRelativeHref(t *testing.T) {
	link := linkparser.Link{
		Href: "/test",
		Text: "test",
	}
	res := LinkToString(link, "http://domain.com")

	assert.EqualValues(t, "http://domain.com/test", res)
}

func TestLinkToStringAbsoluteHref(t *testing.T) {
	link := linkparser.Link{
		Href: "https://test-domain.com/test",
		Text: "test",
	}
	res := LinkToString(link, "http://domain.com")

	assert.EqualValues(t, "https://test-domain.com/test", res)
}

func TestLinksToUrlsNoLinks(t *testing.T) {
	urls := LinksToUrls(nil, "http://test-domain.com")

	assert.Empty(t, urls)
}

func TestLinksToUrlsWithLinks(t *testing.T) {
	links := []linkparser.Link{
		{
			Href: "/test",
		},
		{
			Href: "http://test-domain.com/link-1",
		},
	}
	urls := LinksToUrls(links, "http://test-domain.com")

	assert.NotEmpty(t, urls)
	assert.EqualValues(t, "http://test-domain.com/test", urls[0])
	assert.EqualValues(t, "http://test-domain.com/link-1", urls[1])
}

func TestFilterOutExtrenalDomainUrls(t *testing.T) {
	predicate := func(base string) func(string) bool {
		return func(urlStr string) bool {
			return strings.HasPrefix(urlStr, base)
		}
	}
	base := "https://test-domain.com/"
	filtered := FilterOutExtrenalDomainUrls([]string{
		"https://test-domain.com/test",
		"https://github.com/test",
		"mailto:test@test.com",
		"https://test-domain.com/contact",
	}, predicate(base))

	expectedUrls := []string{
		"https://test-domain.com/test",
		"https://test-domain.com/contact",
	}
	assert.Len(t, filtered, 2)
	assert.Contains(t, expectedUrls, filtered[0])
	assert.Contains(t, expectedUrls, filtered[1])
}
